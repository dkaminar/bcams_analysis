# BCAMs_analysis - A Brief Guide

## Current location of data

Location of BCAM data -> /eos/user/d/dkaminar/data_for_BCAMS_2023 (unique file for every year)

## Goals of the project

1) Monitor each week the stauts of the system by producing plots that represent the changes in the targets movement with respect to time

2) Check for correlations to external factors such as magnet, humidity and temperature

3) Check if left and right half stations are moving uniformly with respect to time

4) 3D represantaiton of the detector to have an estimate of its shape and how it changes over time

5) Achieve precision better than 50μm

6) Understand the banana shapes

7) Study Translations and Rotations

8) Provide feedback to Alignment team

## To do List

1) Have a working system -> Done

2) Check for correlations with the magnet -> Done

3) Develop a framework for weekly monitoring -> Done

4) Understand the jumps in the trend plots when they don't correlate to magnet -> Done

5) Start averaging measurements to increase precision -> Done

6) Find an uncertainty estimation method -> Done

7) Study Translations and Rotations -> Done

8) Estimate the changes in the Survey points -> Done

9) Provide feedback to Alignment team -> Done

10) Improve the 3D representation

11) Implement plots to the Online system

12) Write the Analysis Note

## Description of the project

1) In folders trend_plots_{station} you can find:

    a) Trend plots for each target, standalone and in comparison to the magnet

    b) Trend plots for all targets, in one joint plot

    c) Plots for translation and Rotations

2) In the folder ctrl_plots, you can find all the plots related to the control camera studies

3) You can find the jupyter notebook with the current version of the code. If you want to run it follow these steps:

    a) [create conda environment](https://docs.conda.io/projects/conda/en/latest/user-guide/index.html)
    
    b) use the yaml file located in folder files_conda

4) The gif corresponds to the current 3D represantaiton of the detector and it is constantly improving

## How to access BCAM data and external factors data
First, connect to lxplus and then to lbnx -> ssh lbnx

For BCAM data

```
cd /group/online/inf/BCAM_Run3/LHCb-SciFi-Monitoring/T1/Results
```
txt file -> all_clean_T1.txt 
Modify accordingly for the other stations (T2,T3)

For Control camera

```
cd /group/online/inf/BCAM_Run3/LHCb-SciFi-Monitoring/CTRL/Results
```

For magnet (extract values to a csv - Note: Can support up to 3 months simultaneously, for longer periods create sub files and then merge them. Same for all external factors)

```
/group/online/ecs/util/PVSSExportDirect -s "25-06-2023 00:00:00" -e "26-09-2023 00:00:00" -d "INFMAG:LbMagnet.Current" -o magnet.csv
```

For magnet polarity (Up, Down, OFF)

```
/group/online/ecs/util/PVSSExportDirect -s "25-06-2023 00:00:00" -e "26-09-2023 00:00:00" -d "INFMAG:lbHyst.Polarity" -o magnet_pol.csv
```

For humidity

```
/group/online/ecs/util/PVSSExportDirect -s "25-06-2023 00:00:00" -e "26-09-2023 00:00:00" -d "INFSERVICES:AI_MT_CAVERN_Ambient_near_Balcony.value" -o humidity.csv
```
For temperature - top sensor

```
/group/online/ecs/util/PVSSExportDirect -s "25-06-2023 00:00:00" -e "26-09-2023 00:00:00" -d "INFSERVICES:PT_TE_CAVERN_Ambient_Gantry_top_A.value" -o temp_top.csv
```
For temperature - bottom sensor

```
/group/online/ecs/util/PVSSExportDirect -s "25-06-2023 00:00:00" -e "26-09-2023 00:00:00" -d "INFSERVICES:PT_TE_CAVERN_Ambient_Gantry_bottom_A.value" -o temp_bottom.csv
```

For the data of the temperature of the FEBS
```
/group/online/ecs/util/PVSSExportDirect -s "25-06-2023 00:00:00" -e "26-09-2023 00:00:00" -d "SFDCSMON:CPS_PT100_T2L01Q0_FEBMountingPlate.temp" -o T2L01Q0.csv
```

For the data of the temperature of the SIPM cooling
```
/group/online/ecs/util/PVSSExportDirect -s "25-06-2023 00:00:00" -e "26-09-2023 00:00:00" -d "SFDCSMON:Cool_T2L01Q0.value" -o temp_T2L01Q0.csv
```

Change T_i (1 or 2 for stations - no sensor in T3) , L_ij (01 for 'front side' and 23 for 'back side') and Q_i (0,1,2,3 corresponding to the quadrants - 0 and 2 is C side - 1 and 3 is A side)

For all of them, choose starting and ending date

## Full description of the project along with a manual how to operate the system can be found in the Analysis Note
